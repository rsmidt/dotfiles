;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;=====================================================

[colors]
background = ${xrdb:background:#222}
background-alt = #444
foreground = ${xrdb:foreground:#222}
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40
color1 = ${xrdb:color1}
color2 = ${xrdb:color3}
color5 = ${xrdb:color6}
color6 = ${xrdb:color7}

[bar/example]
monitor = ${env:MONITOR:DP-4}
width = 100%
height = 40
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 2

font-0 = "UW Ttyp0:style=Regular:size=14;1"
font-1 = "Knack Nerd Font:size=20;4"
font-2 = NotoEmoji:size=10;1

modules-left = i3
modules-center = player-mpris-tail
modules-right = alsa xkeyboard eth date powermenu 

tray-position = right
tray-padding = 2
;tray-transparent = true
;tray-background = #0063ff

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground}

label-layout = %layout%

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

ws-icon-0 = 1: Browser;
ws-icon-1 = 2: Development;
ws-icon-2 = 3: Media;
ws-icon-default = 

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %icon%
label-focused-background = ${xrdb:color0}
label-focused-underline = ${xrdb:color5}
label-focused-padding = 2
label-focused-font = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %icon%
label-unfocused-padding = 2
label-unfocused-font = 2

; visible = Active workspace on unfocused monitor
label-visible = %icon%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}
label-visible-font = 2

; urgent = Workspace with urgency hint set
label-urgent = %icon%
label-urgent-background = {colors.background}
label-urgent-padding = 2
label-urgent-font = 2

[module/eth]
type = internal/network
interface = enp0s31f6
interval = 3.0

format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground}
label-connected = %linkspeed%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5

date = " %Y-%m-%d"
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.foreground}

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = muted
label-muted-foreground = #66

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator =
bar-volume-fill =
bar-volume-empty =
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = 
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground}
label-muted = sound muted

bar-volume-width = 8
bar-volume-foreground-0 = ${colors.color5}
bar-volume-foreground-1 = ${colors.color5}
bar-volume-foreground-2 = ${colors.color5}
bar-volume-foreground-3 = ${colors.color5}
bar-volume-foreground-4 = ${colors.color2}
bar-volume-foreground-5 = ${colors.color1}
bar-volume-foreground-6 = ${colors.color1}
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-fill = ─
bar-volume-empty = ─
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.foreground}
label-close =  cancel
label-close-foreground = ${colors.foreground}
label-separator = |
label-separator-foreground = ${colors.foreground}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[module/player-mpris-tail]
type = custom/script
exec = ~/polybar-scripts/player-mpris-tail/player-mpris-tail.py
tail = true
click-left = ~/polybar-scripts/player-mpris-tail/player-ctrl.sh previous
click-right = ~/polybar-scripts/player-mpris-tail/player-ctrl.sh next
click-middle = ~/polybar-scripts/player-mpris-tail/player-ctrl.sh play-pause

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
